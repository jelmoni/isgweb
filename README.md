# ISG web extension

This is a modification of original software from _Michael Schumann_. See file [README.txt](README.txt).

## Modifications
- hard code IP of ISGWEB
- Adapt all to ENGL - original is working only if language ist German
- small changes for my "custom installation"

### todo 
It's work but not really integrated in my homesystem - loxone usw.

software is written by _Michael Schumann_

contact for modifications: [sw-develop@jelmoni.net](mailto:sw-develop@jelmoni.net?subject=isgweb modification)
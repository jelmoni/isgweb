#######################################
## Development by Michael Schumann
## www.MD-Technologie.de
#######################################
## Only for private use
#######################################

Eine detailierte Beschreibung befindet sich in meinem Blog:
https://www.md-technologie.de/baublog/

Vorraussetzungen:
- Eine Fritzbox oder ein Router mit �hnlichen Funktionen
- Eine Website mit PHP und einer Datenbank (z.B. MySQL)
- Ein Cronjob (bieten viele Websitehoster an)
- ISG web Version 10.2.0 ODER ISG web mit EMI Update Version 2.2.0
- Grundlegende Webentwicklungskenntnisse / PHP Kenntnisse

Beschreibung und Funktion der einzelnen Dateien:

========================================
homeIP.txt
Beinhaltet die IP4 Adresse des ISG web bzw. des Routers, siehe Beschreibung zuhause.php.

========================================
index.php
Diese Datei dient dem Verzeichnisschutz und bindet die show_lwp.php ein um sie anzuzeigen. Alternativ kann die show_lwp.php auch direkt aufgerufen werden.

========================================
isgweb.sql
SQL dump der Datenbankstruktur. Damit kann das Datenbankschema (z.B. mit PHPmyAdmin) aufgesetzt werden.

========================================
reading.php
In dieser Datei befinden sich die Funktionen zum Auslesen der ISG web Startseite, Infoseite und Sommerbetrieb. Die Daten werden ausgelesen und in eine Datenbank geschrieben. Dazu ist zwingend die korrekt konfiguriete Datei sqlconn.php.inc erforderlich. Wenn sich die ISG web Versionen von den oben genannten unterscheiden kann es dazu f�hren, dass die Seiten nicht korrekt/unvollst�ndig/gar nicht eingelesen werden.
Sollte man bereits �ber eine statische URL f�r das ISG web verf�gen, muss die Zeile 345 angepasst werden. F�r das Heinnetzwerk kann die lokale IP in die Datei homeIP.txt eingetragen werden.

========================================
show_lwp.php
Mit Hilfe dieser Datei k�nnen die Daten aus der Datenbank ausgelesen und per google Charts angezeigt werden. Es k�nnen verschiedene Zeitr�ume ausgew�hlt werden. Dazu ist zwingend die korrekt konfiguriete Datei sqlconn.php.inc erforderlich.

========================================
isg_current_temp.sql
SQL dump der Datenbankstruktur. Damit kann das Datenbankschema (z.B. mit PHPmyAdmin) aufgesetzt werden.

========================================
readingisg_temp.php
In dieser Datei befinden sich die Funktionen zum Auslesen der ISG Warmwassertemperatur und der Heiztemperatur (IST-Temperatur, SOLL-Temperatur, Vorlauf-IST-Temperatur, Puffer-SOLL-Temperatur) von der Anlagenseiten. Diese Funktion befindet sich in einer seperaten Datei, damit sie in einem anderen Rhythmus als die reading.php ausgef�hrt werden kann. W�hren die allgemeinen Werte immer nur Tageswerte sind, �ndert sich die Temperatur laufend. F�r eine Nachvollziehbarkeit empfiehlt sich eine Ausf�hrung jede Stunde oder alle 15 Minuten. Wie in der reading.php sind alle Konfigurationsdaten in der sqlconn.php.inc enthalten. Wenn sich die ISG web Versionen von den oben genannten unterscheiden kann es dazu f�hren, dass die Seiten nicht korrekt/unvollst�ndig/gar nicht eingelesen werden.
Sollte man bereits �ber eine statische URL f�r das ISG web verf�gen, muss die Zeile 155 angepasst werden. F�r das Heinnetzwerk kann die lokale IP in die Datei homeIP.txt eingetragen werden.

========================================
show_temp.php
Mit Hilfe dieser Datei k�nnen die Daten aus der Datenbank ausgelesen und per google Charts angezeigt werden. F�r die Anzeige der Warmwassertemperatur und der Heiztemperatur kann ein immer ein einzelner Tag ausgew�lt werden. Dazu ist zwingend die korrekt konfiguriete Datei sqlconn.php.inc erforderlich.

========================================
sqlconn.php.inc
Generelle Config Datei. Hier werden die Verbindungsdaten f�r die SQL Datenbank konfiguriert.
Au�erdem sind hier die Logindaten f�r das ISGweb hinterlegt.

========================================
isg.css
Layoutdatei.

========================================
zuhause.php
Die Funktionsweise wird detailiert hier beschrieben: http://nicht-traeumen-sondern-machen.de/RaspberryPi_Basteleien/DynDNS_mit_Fritzbox.php
Wird die Datei ohne Parameter aufgerufen (also nur zuhause.php) wird an die in der Datei homeIP.txt hinterlegte IP Adresse weitergeleitet.
�ber folgenden Aufruf kann die IP4 Adresse in die Datei homeIP.txt geschrieben werden: www.MeineDomain.de/ordner/filename.php?pass=Passwort&meineip=127.0.0.1
Das �bergebene Passwort muss in Zeile 13 defniert werden.
In der Fritzbox muss anstatt der IP eine Variable angegeben werden, die URL lautet dann wie folgt:
www.MeineDomain.de/ordner/filename.php?pass=Passwort&meineip=<ipaddr>


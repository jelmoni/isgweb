<?php require 'sqlconn.inc.php';?>
<?php

function checkEntryDate($Name) {
	$conn = connectoToDB();

	$datum = date("Ymd",strtotime("-1 day"));
	
	$sql = "SELECT * FROM isgweb WHERE Name = '".$Name."' AND Datum = '".$datum."'";
	$result = $conn->query($sql);
	
	if (!empty($result) && $result->num_rows > 0) {
		$conn->close();
		return true;
	}
	else {
		$conn->close();
		return false;	
	}
}

function writeToDB($Name, $Wert, $Einheit) {
	$conn = connectoToDB();

	$Datum = date("Ymd",strtotime("-1 day"));
	$sql = "INSERT INTO isgweb (Name, Datum, Wert, Einheit)
	VALUES ('".$Name."', '".$Datum."', '".$Wert."', '".$Einheit."')";
echo $sql."<br>";
	
	if (!checkEntryDate($Name)) {
		if($conn->query($sql) === TRUE) {
			echo "New record created successfully";
		} else {
			echo "Error: " . $sql . "<br>" . $conn->error;
		}
	}
	
	$conn->close();
}

function startsWith($haystack, $needle)
{
     $length = strlen($needle);
     return (substr($haystack, 0, $length) === $needle);
}

function sonderzeichen($string)
{
	$search = array("�", "�", "�", "�", "�", "�", "�", "�");
	$replace = array("Ae", "Oe", "Ue", "ae", "oe", "ue", "ss", "");

	$search2 = array();  // ---- neues Such-Array im UTF-8 Format ----
	foreach($search as $item) { 
		$search2[] = utf8_encode($item); 
	} 
	return str_replace($search2, $replace, $string);
}

function getWPInfosite($content) {
/*	$agent = "IE10";
	$curl = curl_init();
	$header[] = "Accept: text/vnd.wap.wml, *.*";

	curl_setopt($curl, CURLOPT_URL, $url);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($curl, CURLOPT_COOKIEJAR, COOKIE_FILE);
	curl_setopt($curl, CURLOPT_USERAGENT, USER_AGENT);

	$content = curl_exec($curl);
*/	
	if($content === false): echo "Fehler";endif;
//	curl_close($curl);

	$dom = new DOMDocument();
	libxml_use_internal_errors(true);
	$dom->loadHTML($content);
	libxml_use_internal_errors(false);

	$xpath = new DOMXpath($dom);

	//internal VARs
	//W�rmemenge
	$VD_HEIZEN_TAG = 0;
	$VD_HEIZEN_SUMME = 0;
	$VD_WARMWASSER_TAG = 0;
	$VD_WARMWASSER_SUMME = 0;
	$NHZ_HEIZEN_SUMME = 0;
	$NHZ_WARMWASSER_SUMME = 0;

	//Starts
	$VERDICHTER = 0;

	//Leistungsaufnahme
	$VD_HEIZEN_TAG = 0;
	$VD_HEIZEN_SUMME = 0;
	$VD_WARMWASSER_TAG = 0;
	$VD_WARMWASSER_SUMME = 0;

	$VD_HEIZEN = 0;
	$VD_WARMWASSER = 0;
	$VD_ABTAUEN = 0;
	$NHZ_1 = 0;
	$NHZ_2 = 0;
	$NHZ_1_2 = 0;
	$ZEIT_ABTAUEN = 0;
	$STARTS_ABTAUEN = 0;

	$elements = $dom->getElementsByTagName('tr');

	$Name = "";
	$Wert = "";
	$Einheit = "";
	$headingPrefix = "";
	
	foreach($elements as $tr) {
		$i=0;
		foreach($tr->childNodes as $td) {	
			if($td->nodeName =="th") {
				$headingPrefix = substr(sonderzeichen($td->nodeValue),0, 2);			
				echo "<h1>".$headingPrefix."</h1>";
			}
			if($td->nodeName =="td") {
				//echo "##".$td->nodeName."&".$td->nodeValue."&".$td->getAttribute('class')."##";
				$tdvalue = trim($td->nodeValue);
				if(startsWith($td->getAttribute('class'),"key") && !empty($tdvalue)) {
						$Name = $headingPrefix."_".str_replace(" ", "_", $tdvalue);		
						echo "<h2>".$Name."</h2>";								
				}
				if(startsWith($td->getAttribute('class'),"value") && !empty($tdvalue)) {
						$values = explode(" ", $tdvalue);
						if(count($values) > 1) {
							echo "<p>Value: ".$values[0]." unit: ".$values[1]."</p>";
							$Wert = $values[0];
							$Einheit = $values[1];
						}
						else {
							echo "<p>".$tdvalue."#</p>"; 
							$Wert = $tdvalue;
							$Einheit = "";
						}
				}
			}		
		}
		writeToDB($Name, $Wert, $Einheit);
	}
}

function getWPstartsite($content) {
	//$content = file_get_contents($url);
	//var_dump( htmlspecialchars($content, ENT_QUOTES,"UTF-8") ); 
/*	
	$curl = curl_init();
	$header[] = "Accept: text/vnd.wap.wml, *.*";

	curl_setopt($curl, CURLOPT_URL, $url);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
*/
	
	//$content = curl_exec($curl);
	if($content === false): echo "Fehler";endif;
//	curl_close($curl);

	$dom = new DOMDocument();
	libxml_use_internal_errors(true);
	$dom->loadHTML($content);
	libxml_use_internal_errors(false);

	$xpath = new DOMXpath($dom);
	$elements = $dom->getElementsByTagName('script');
	$scriptContent = $elements->item(19)->nodeValue;

	$charts = preg_split('/\[\[(.?)/', $scriptContent);

	for($i=1; $i<6; $i++){
		$numbers = explode("]]", $charts[$i]);
		$dates = explode("[", $numbers[0]);
		$unit = explode(",", $dates[6]);
		switch($i) {
			case 1: 
				writeToDB("Aussentemperatur_min", $unit[1], "C");
				break;
			case 2: 
				writeToDB("Aussentemperatur_mittel", $unit[1], "C");
				break;
			case 3: 
				writeToDB("Aussentemperatur_max", $unit[1], "C");
				break;
			case 4: 
				writeToDB("Heizenergie", $unit[1], "kWh");
				break;
			case 5: 
				writeToDB("Warmwasserenergie", $unit[1], "kWh");
				break;
		}
	}

}

function getWPsommer($content) {
	if($content === false): echo "Fehler";endif;
//	curl_close($curl);

	$dom = new DOMDocument();
	libxml_use_internal_errors(true);
	$dom->loadHTML($content);
	libxml_use_internal_errors(false);

	//$xpath = new DOMXpath($dom);
	$elements = $dom->getElementById('val105');
	$scriptContent = $elements->nextSibling;	
	$rest = substr($scriptContent->nodeValue, -11, 4); 
	writeToDB("Sommerbetrieb", $rest, "C");
}

function loginToServer($url, $url_start, $url_wpinfo, $url_Sommer) {
	//Quelle: https://thisinterestsme.com/php-login-to-website-with-curl/
	 
	//URL of the login form.
	define('LOGIN_FORM_URL', $url);
	 
	//Login action URL. Sometimes, this is the same URL as the login form.
	define('LOGIN_ACTION_URL', $url);
	 
	 
	//An associative array that represents the required form fields.
	//You will need to change the keys / index names to match the name of the form
	//fields.
	$postValues = array(
		'user' => USERNAME,
		'pass' => PASSWORD
	);
	 
	//Initiate cURL.
	$curl = curl_init();
	 
	//Set the URL that we want to send our POST request to. In this
	//case, it's the action URL of the login form.
	curl_setopt($curl, CURLOPT_URL, LOGIN_ACTION_URL);
	 
	//Tell cURL that we want to carry out a POST request.
	curl_setopt($curl, CURLOPT_POST, true);
	 
	//Set our post fields / date (from the array above).
	curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($postValues));
	 
	//We don't want any HTTPS errors.
	curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
	curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
	 
	//Where our cookie details are saved. This is typically required
	//for authentication, as the session ID is usually saved in the cookie file.
	curl_setopt($curl, CURLOPT_COOKIEJAR, COOKIE_FILE);
	 
	//Sets the user agent. Some websites will attempt to block bot user agents.
	//Hence the reason I gave it a Chrome user agent.
	curl_setopt($curl, CURLOPT_USERAGENT, USER_AGENT);
	 
	//Tells cURL to return the output once the request has been executed.
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	 
	//Allows us to set the referer header. In this particular case, we are 
	//fooling the server into thinking that we were referred by the login form.
	curl_setopt($curl, CURLOPT_REFERER, LOGIN_FORM_URL);
	 
	//Do we want to follow any redirects?
	curl_setopt($curl, CURLOPT_FOLLOWLOCATION, false);
	 
	//Execute the login request.
	curl_exec($curl);
	 
	//Check for errors!
	if(curl_errno($curl)){
		throw new Exception(curl_error($curl));
	}
	 
	//############### start reading start site
	//We should be logged in by now. Let's attempt to access a password protected page
	curl_setopt($curl, CURLOPT_URL, $url_start);
	 
	//Use the same cookie file.
	curl_setopt($curl, CURLOPT_COOKIEJAR, COOKIE_FILE);
	 
	//Use the same user agent, just in case it is used by the server for session validation.
	curl_setopt($curl, CURLOPT_USERAGENT, USER_AGENT);
	 
	//We don't want any HTTPS / SSL errors.
	curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
	curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
	 
	//Execute the GET request and print out the result.
	$content = curl_exec($curl);
	echo "start reading start site<br />";
	echo $url_start."<br>";
	getWPstartsite($content);
	
	//############### start reading Info site
	//We should be logged in by now. Let's attempt to access a password protected page
	curl_setopt($curl, CURLOPT_URL, $url_wpinfo);
	 
	//Use the same cookie file.
	curl_setopt($curl, CURLOPT_COOKIEJAR, COOKIE_FILE);
	 
	//Use the same user agent, just in case it is used by the server for session validation.
	curl_setopt($curl, CURLOPT_USERAGENT, USER_AGENT);
	 
	//We don't want any HTTPS / SSL errors.
	curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
	curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
	 
	//Execute the GET request and print out the result.
	$content = curl_exec($curl);
	
	echo "start reading INFO site<br />";
	echo $url_wpinfo."<br>";
	getWPInfosite($content);
	
	//############### start reading Sommer site
	//We should be logged in by now. Let's attempt to access a password protected page
	curl_setopt($curl, CURLOPT_URL, $url_Sommer);
	 
	//Use the same cookie file.
	curl_setopt($curl, CURLOPT_COOKIEJAR, COOKIE_FILE);
	 
	//Use the same user agent, just in case it is used by the server for session validation.
	curl_setopt($curl, CURLOPT_USERAGENT, USER_AGENT);
	 
	//We don't want any HTTPS / SSL errors.
	curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
	curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
	 
	//Execute the GET request and print out the result.
	$content = curl_exec($curl);
	
	echo "start reading INFO site<br />";
	echo $url_Sommer."<br>";
	getWPsommer($content);
	
}

//####################### Main ######################

//Read current IP
//$dyntxt = "homeIP.txt";

//if (file_exists($dyntxt)){
//	$a = fopen("$dyntxt", "r+"); //Kein Passwort, dann nur IP auslesen und weiterleiten
//	$dynamicip = fread($a,filesize($dyntxt));


	//Variablen
	//Set a user agent. This basically tells the server that we are using Chrome ;)
	define('USER_AGENT', 'Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.2309.372 Safari/537.36'); 
	//Where our cookie information will be stored (needed for authentication).
	define('COOKIE_FILE', 'cookie.txt');
	
	$url = "http://192.168.78.186";
	$url_start = "http://192.168.78.186/?s=0";
	$url_wpinfo = "http://192.168.78.186/?s=1,1";
	$url_Sommer = "http://192.168.78.186/?s=4,0,1";
	
	echo "Try to login to $url<br />";
	loginToServer($url, $url_start, $url_wpinfo, $url_Sommer);
//} else echo "No current IP found";
?>


<?php require 'sqlconn.inc.php';?>

<?php
function getDBData($Name, $DatumMin, $DatumMax) {
	$conn = connectoToDB();

	$datum = (date("Ymd")-date(1));
	
	$sql = "SELECT * FROM isgweb WHERE Name = '".$Name."' AND Datum >= '".$DatumMin."' AND Datum <= '".$DatumMax."' ORDER BY Datum ASC";
	//echo $sql;
	$result = $conn->query($sql);
	
	if (!empty($result) && $result->num_rows > 0) {
		//echo $result->num_rows;
		$conn->close();
		return $result;
	}
	else {
		$conn->close();
		//echo "Error";
		return "";	
	}
}

	if(isset($_POST['DatumMin'])) $DatumMin = $_POST['DatumMin'];
	if(empty($DatumMin)) $DatumMin = date("Y-m-d",strtotime("-1 month")); else $DatumMin = date($DatumMin);
	
	if(isset($_POST['DatumMax'])) $DatumMax = $_POST['DatumMax'];
	if(empty($DatumMax)) $DatumMax = date("Y-m-d"); else $DatumMax = date($DatumMax);

	//echo $DatumMin;
	//echo "<br/>";
	//echo $DatumMax;
?>

<html>
  <head>
	<link rel="stylesheet" type="text/css" href="isg.css" media="screen" />
    <!--Load the AJAX API-->
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
      // Load the Visualization API and the corechart package.
      google.charts.load('current', {'packages':['corechart']});

      // Set a callback to run when the Google Visualization API is loaded.
      //google.charts.setOnLoadCallback(drawChart);

      // Callback that creates and populates a data table,
      // instantiates the pie chart, passes in the data and
      // draws it.
      function drawChart(title, einheit, daten) {
		
		var data = new google.visualization.DataTable();
        data.addColumn('date', 'Datum');
        data.addColumn('number', einheit);
		         
		daten.forEach(function(datensatz, index, array) {
			console.log(datensatz, index);
			data.addRows([[new Date(datensatz[0], datensatz[1]-1, datensatz[2]), datensatz[3]]]);			
		});		

		titleDisplay = title.replace("WA_", "").replace("ST_", "").replace("LE_", "").replace("LA_", "").replace("_", " ").replace("_", " ");
		console.log(titleDisplay);
		
        // Set chart options
        var options = {'title':titleDisplay,
                       width:600,
                       height:300,
					   backgroundColor: 'transparent',
						hAxis: {
							format: ' dd.MM.yyyy',
							title: 'Zeit'
						},
						vAxis: {
							title: einheit,
//							minValue:0
						}					   
					   };

        // Instantiate and draw our chart, passing in some options.
        var chart = new google.visualization.LineChart(document.getElementById('chart_div_'+title));
        chart.draw(data, options);
      }
	  
      function drawChartTemperatur(title, einheit, daten) {
		
		var data = new google.visualization.DataTable();
        data.addColumn('date', 'Datum');
        data.addColumn('number', "Max");
        data.addColumn('number', "Mittel");
        data.addColumn('number', "Min");
		data.addColumn('number', "Sommerbetrieb");
		         
		daten.forEach(function(datensatz, index, array) {
			console.log(datensatz, index);
			data.addRows([[new Date(datensatz[0], datensatz[1]-1, datensatz[2]), datensatz[5], datensatz[4], datensatz[3], datensatz[6]]]);			
		});		
	
        // Set chart options
        var options = {title:title,
                       width:600,
                       height:300,
					   colors: ['red', 'green', 'blue', 'yellow'],
   					   backgroundColor: '#DBF6FC',
						hAxis: {
							format: ' dd.MM.yyyy',
							title: 'Zeit'
						},
						vAxis: {
							title: einheit,
							minValue:0
						}					   
					   };

        // Instantiate and draw our chart, passing in some options.
        var chart = new google.visualization.LineChart(document.getElementById('chart_div_Aussentemperatur'));
        chart.draw(data, options);
      }	  
    </script>
	
	<script>	

 <?php 
	$temperaturen = array(
		"Aussentemperatur_min", 
		"Aussentemperatur_mittel",
		"Aussentemperatur_max",
		"Sommerbetrieb"
	);
	$tempDaten = "";
 	foreach($temperaturen AS $wert) {
		$daten = getDBData($wert, $DatumMin, $DatumMax);
		$einheit = "";
		$wert = str_replace("/","_",$wert);
		$i=0;
		if (!empty($daten)) {
			while($row = $daten->fetch_assoc()) {
			//echo "Name: " . $row["Name"]. " - Datum: " . $row["Datum"]. " - " . $row["Wert"]. "- " . $row["Einheit"]. "<br>";
			$datum = explode("-", $row["Datum"]);
			if($wert =="Aussentemperatur_min") {
				if(empty($tempDaten)) {
					$tempDaten = array(array($datum[0], $datum[1], $datum[2], $row["Wert"]));
				} else {
					array_push($tempDaten, array($datum[0], $datum[1], $datum[2], $row["Wert"]));
				}
			} else {
					array_push($tempDaten[$i], $row["Wert"]);
					$i++;				
			}
				
				
			}
		}
	}	

	echo "var daten_aussen = [";
	foreach($tempDaten as $tempdata) {
			echo "[".$tempdata[0].", ".$tempdata[1].", ".$tempdata[2].", ".$tempdata[3].", ".$tempdata[4].", ".$tempdata[5].", ".str_replace(",",".",$tempdata[6])."],";		
	}
    echo "];\r\n";
	echo "google.charts.setOnLoadCallback(function () {\r\n";
	echo "drawChartTemperatur('Aussentemperatur', 'C', daten_aussen);\r\n";
	echo "});\r\n";	  
 
 
 //$Heizenergie = "Heizenergie";
 //$Warmwasserenergie = "Warmwasserenergie";
 
 $werte = array(
           "Heizenergie",
           "Warmwasserenergie",
	   "PO_COMPRESSOR_HEATING_DAY",
           "AM_BH_DHW_TOTAL",
	   "AM_BH_HEATING_TOTAL",
	   "AM_COMPRESSOR_DHW_DAY",
	   "AM_COMPRESSOR_DHW_TOTAL",
	   "AM_COMPRESSOR_HEATING_DAY",
	   "AM_COMPRESSOR_HEATING_TOTAL".
	   "Aussentemperatur_max",
	   "Aussentemperatur_min",
	   "Aussentemperatur_mittel",
	   "PO_COMPRESSOR_DHW_DAY",
	   "PO_COMPRESSOR_DHW_TOTAL",
	   "PO_COMPRESSOR_HEATING_TOTAL",
	   "PR_FROST_PROTECTION_TEMP",
	   "PR_HIGH_PRESSURE",
	   "PR_HOT_GAS_TEMPERATURE",
	   "PR_LOW_PRESSURE",
	   "RU_BH_1",
	   "RU_BH_1/2",
	   "RU_BH_2",
	   "RU_DEFROST_HEATER",
	   "RU_RNT_COMP_1_DHW",
	   "RU_RNT_COMP_1_HEA",
	   "Sommerbetrieb"
//	"Heizenergie",
//	"Warmwasserenergie",
//	"WA_VD_HEIZEN_TAG",
//	"WA_VD_HEIZEN_SUMME",
//	"WA_VD_WARMWASSER_TAG",
//	"WA_VD_WARMWASSER_SUMME",
//	"WA_NHZ_HEIZEN_SUMME",
//	"WA_NHZ_WARMWASSER_SUMME",
//	"ST_VERDICHTER",
//	"LE_VD_HEIZEN_TAG",
//	"LE_VD_HEIZEN_SUMME",
//	"LE_VD_WARMWASSER_TAG",
//	"LE_VD_WARMWASSER_SUMME",
//	"LA_VD_HEIZEN",
//	"LA_VD_WARMWASSER",
//	"LA_VD_ABTAUEN",
//	"LA_NHZ_1",
//	"LA_NHZ_2",
//	"LA_NHZ_1/2",
//	"LA_ZEIT_ABTAUEN",
//	"LA_STARTS_ABTAUEN"
	);



	
	foreach($werte AS $wert) {
		$daten = getDBData($wert, $DatumMin, $DatumMax);
		$einheit = "";
		$wert = str_replace("/","_",$wert);
		echo "var daten_".$wert." = [";
		if (!empty($daten)) {
			while($row = $daten->fetch_assoc()) {
				// echo "Name: " . $row["Name"]. " - Datum: " . $row["Datum"]. " - " . $row["Wert"]. "- " . $row["Einheit"]. "<br>";
				$datum = explode("-", $row["Datum"]);
				echo "[".$datum[0].", ".intval($datum[1]).", ".intval($datum[2]).", ".str_replace(",",".",$row["Wert"])."],";
				$einheit = $row["Einheit"];
			};
		}		
	   echo "];\r\n";
		echo "google.charts.setOnLoadCallback(function () {\r\n";
		echo "drawChart('".$wert."', '".$einheit."', daten_".$wert.");\r\n";
		echo "});\r\n";	   
	}	
?>
			
		</script>	
		
  </head>


  
  <body>
<?php include "navigation.php"; ?>  
<div style="padding:20px; border-bottom:2px solid green; ">
<div style="display:inline;">
	<form action="show_lwp.php" method="post" style="display:inline;">
		<input type="hidden" name="DatumMin" value="<?php echo date("Y-m-d", strtotime('monday last week')); ?>" />
		<input type="hidden" name="DatumMax" value="<?php echo date("Y-m-d", strtotime('sunday last week')); ?>" />
		<input type="submit" value=" letzte Woche ">
	</form>
	<form action="show_lwp.php" method="post" style="display:inline;">
		<input type="hidden" name="DatumMin" value="<?php echo date("Y-m-d", strtotime('monday this week')); ?>" />
		<input type="hidden" name="DatumMax" value="<?php echo date("Y-m-d");?>" />
		<input type="submit" value=" aktuelle Woche ">
	</form>
	<form action="show_lwp.php" method="post" style="display:inline;">
		<input type="hidden" name="DatumMin" value="<?php echo date("Y-m-01", strtotime("-1 month")) ;?>" />
		<input type="hidden" name="DatumMax" value="<?php echo date("Y-m-d", strtotime("last day of this month -1 month")) ;?>" />
		<input type="submit" value=" letzter Monat ">
	</form>
	<form action="show_lwp.php" method="post" style="display:inline;">
		<input type="hidden" name="DatumMin" value="<?php echo date("Y-m-01");?>" />
		<input type="hidden" name="DatumMax" value="<?php echo date("Y-m-d");?>" />
		<input type="submit" value=" aktueller Monat ">
	</form>
	<form action="show_lwp.php" method="post" style="display:inline-block;">
		<input type="hidden" name="DatumMin" value="<?php echo date("Y-01-01");?>" />
		<input type="hidden" name="DatumMax" value="<?php echo date("Y-m-d");?>" />
		<input type="submit" value=" aktuelles Jahr ">
	</form>
</div>
<div style="display:inline-block; min-height:30px;">
<form action="show_lwp.php" method="post" style="display:inline;padding-left:60px;">
	<input type="date" name="DatumMin" value="<?php echo date("Y-01-01");?>" />
	<input type="date" name="DatumMax" value="<?php echo date("Y-m-d");?>" />
	<input type="submit" value=" Datums Auswahl ">
</form>
</div>
</div>

<div style="width:100%; border-bottom:2px solid green;text-align:center;">
<span style="width:400px; text-align:left;">Von: <?php echo date("d.m.Y",strtotime($DatumMin)) ?></span> - 
<span style="width:400px; text-align:right">Bis: <?php echo date("d.m.Y",strtotime($DatumMax)) ?></span>
</div>
  
    <!--Div that will hold the pie chart-->
    <div id="chart_div_Aussentemperatur" style="background-color:#DBF6FC;"></div>

	<div style="width:100%; display:inline-block; background-color:#E4E4E4;" >
		<div class="chart" id="chart_div_Heizenergie"></div>
		<div class="chart" id="chart_div_Warmwasserenergie"></div>
	</div>

	<div>
		<div class="greenheading">W&auml;rmemenge</div>
		<div class="chart" id="chart_div_AM_COMPRESSOR_HEATING_DAY"></div>
		<div class="chart" id="chart_div_AM_COMPRESSOR_HEATING_TOTAL"></div>
		<div class="chart" id="chart_div_AM_COMPRESSOR_DHW_DAY"></div>
		<div class="chart" id="chart_div_AM_COMPRESSOR_DHW_TOTAL"></div>
		<div class="chart" id="chart_div_AM_BH_HEATING_TOTAL"></div>
		<div class="chart" id="chart_div_AM_BH_DHW_TOTAL"></div>
	</div>

	<div>	
		<div class="greenheading">Starts</div>
		<div class="chart"id="chart_div_ST_VERDICHTER"></div>
	</div>
	
	<div>	
		<div class="greenheading">Leistungsaufnahme</div>
		<div class="chart" id="chart_div_PO_COMPRESSOR_HEATING_DAY"></div>
		<div class="chart" id="chart_div_PO_COMPRESSOR_HEATING_TOTAL"></div>
		<div class="chart" id="chart_div_PO_COMPRESSOR_DHW_DAY"></div>
		<div class="chart" id="chart_div_PO_COMPRESSOR_DHW_TOTAL"></div>
	</div>
				
	<div>	
		<div class="greenheading">Laufzeit</div>
		<div class="chart" id="chart_div_RU_RNT_COMP_1_HEA"></div>
		<div class="chart" id="chart_div_RU_RNT_COMP_1_DHW"></div>
		<div class="chart" id="chart_div_RU_DEFROST_HEATER"></div>
		<div class="chart" id="chart_div_RU_BH_1"></div>
		<div class="chart" id="chart_div_RU_BH_2"></div>
		<div class="chart" id="chart_div_RU_BH_1/2"></div>
		<div class="chart" id="chart_div_LA_ZEIT_ABTAUEN"></div>
		<div class="chart" id="chart_div_LA_STARTS_ABTAUEN"></div>
	</div>
	<div style="text-align:left;">
		<div class="greenheading">Legende</div>
		<b>W&auml;rmemenge</b>
		<dl>
			<li>VD HEIZEN TAG: W&auml;rmemenge des Verdichters im Heizbetrieb seit 0:00 Uhr des aktuellen Tages.</li>
			<li>VD HEIZEN SUMME: Gesamtsumme der W&auml;rmemenge des Verdichters im Heizbetrieb.</li>
			<li>VD WARMWASSER TAG: W&auml;rmemenge des Verdichters im Warmwasserbetrieb seit 0:00 Uhr des aktuellen Tages.</li>
			<li>VD WARMWASSER SUMME: Gesamtsumme der W&auml;rmemenge des Verdichters im Warmwasserbetrieb.</li>
			<li>NHZ HEIZEN SUMME: Gesamtsumme der W&auml;rmemenge der Nachheizstufen im Heizbetrieb.</li>
			<li>NHZ WARMWASSER SUMME: Gesamtsumme der W&auml;rmemenge der Nachheizstufen im Warmwasserbetrieb.</li>

		</dl>

		<b>Leistungsaufnahme</b>
		<dl>
			<li>VD HEIZEN TAG: Elektrische Leistung des Verdichters im Heizbetrieb seit 0:00 Uhr des aktuellen Tages.</li>
			<li>VD HEIZEN SUMME: Gesamtsumme der Elektrischen Leistung des Verdichters im Heizbetrieb.</li>
			<li>VD WARMWASSER TAG: Elektrische Leistung des Verdichters im Warmwasserbetrieb seit 0:00 Uhr des aktuellen Tages.</li>
			<li>VD WARMWASSER SUMME: Gesamtsumme der Elektrischen Leistung des Verdichters im Warmwasserbetrieb.</li>
		</dl>
		<b>Laufzeiten</b>
		<dl>
			<li>VD HEIZEN: Laufzeit des Verdichters im Heizbetrieb.</li>
			<li>VD WARMWASSER: Laufzeit des Verdichters im Warmwasserbetrieb.</li>
			<li>VD ABTAUEN: Laufzeit des Verdichters im Abtaubetrieb</li>
			<li>NHZ 1: Laufzeit der elektrischen Not-/Zusatzheizung in der Nachheizstufe 1</li>
			<li>NHZ 2: Laufzeit der elektrischen Not-/Zusatzheizung in der Nachheizstufe 2</li>
			<li>NHZ 1 / 2: Laufzeit der elektrischen Not-/Zusatzheizung in den Nachheizstufen 1 und 2</li>					
		</dl>
	</div>
	</div>	
	</body>
</html>

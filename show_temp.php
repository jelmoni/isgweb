<?php require 'sqlconn.inc.php';?>

<?php

	if(isset($_POST['Datum'])) $Datum = $_POST['Datum'];
	//if(empty($Datum)) $Datum = date("Y-m-d",strtotime("-1 day")); else $Datum = date($Datum);
	if(empty($Datum)) $Datum = date("Y-m-d"); else $Datum = date($Datum);
	
	//echo $DatumMin;
	//echo "<br/>";
	//echo $DatumMax;


/*
function getDBData_isgwwtemp($Datum) {
	$conn = connectoToDB();

	//$datum = (date("Y-m-d")-date(1));
	$sql = "SELECT * FROM isgwwtemp WHERE Datum >= '".$Datum." 00:00:00' AND Datum <= '".$Datum." 23:59:59' ORDER BY Datum ASC";
	//echo $sql;
	$result = $conn->query($sql);
	
	if (!empty($result) && $result->num_rows > 0) {
		//echo $result->num_rows;
		$conn->close();
		return $result;
	}
	else {
		$conn->close();
		//echo "Error";
		return "";	
	}
}
*/

function getDBData($Name, $Datum) {
	$conn = connectoToDB();

	//$datum = (date("Y-m-d")-date(1));
	$sql = "SELECT * FROM isg_current_temp WHERE Name = '".$Name."' AND Datum >= '".$Datum." 00:00:00' AND Datum <= '".$Datum." 23:59:59' ORDER BY Datum ASC";
	//echo $sql;
	$result = $conn->query($sql);
	
	if (!empty($result) && $result->num_rows > 0) {
		//echo $result->num_rows;
		$conn->close();
		return $result;
	}
	else {
		$conn->close();
		//echo "Error";
		return "";	
	}
}
?>

<html>
  <head>
	<link rel="stylesheet" type="text/css" href="isg.css" media="screen" />
    <!--Load the AJAX API-->
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
      // Load the Visualization API and the corechart package.
      google.charts.load('current', {'packages':['corechart']});

      // Set a callback to run when the Google Visualization API is loaded.
      //google.charts.setOnLoadCallback(drawChart);

      // Callback that creates and populates a data table,
      // instantiates the pie chart, passes in the data and
      // draws it.
	  
	  /*
      function drawChart(title, daten) {
		
		var data = new google.visualization.DataTable();
        data.addColumn('date', 'Datum');
        data.addColumn('number', "C");
		data.addColumn({type: 'string', role: 'tooltip'});
		         
		daten.forEach(function(datensatz, index, array) {
			console.log(datensatz, index);
			time = datensatz[3]+":"+((datensatz[4]==0)?"00":datensatz[4])+" Uhr | "+ datensatz[5]+" C";
			//data.addRows([[new Date(datensatz[0], datensatz[1]-1, datensatz[2]), datensatz[6]]]);			
			data.addRows([[new Date(datensatz[0], datensatz[1]-1, datensatz[2], datensatz[3], datensatz[4]), datensatz[5], time]]);			
		});		
		
        // Set chart options
        var options = {'title':title,
                       width:600,
                       height:300,
					   colors: ['orange', 'green', 'blue', 'yellow'],
					   backgroundColor: 'transparent',
						hAxis: {
							format: 'HH:mm',
							title: 'Zeit'
						},
						vAxis: {
							title: 'C',
//							minValue:0
						}					   
					   };

        // Instantiate and draw our chart, passing in some options.
        var chart = new google.visualization.LineChart(document.getElementById('chart_div_wwtemp'));
        chart.draw(data, options);
      }
	  */
	  
      function drawChart(title, einheit, daten) {
		
		var data = new google.visualization.DataTable();
        data.addColumn('date', 'Datum');
        data.addColumn('number', "C");
		data.addColumn({type: 'string', role: 'tooltip'});
		         
		daten.forEach(function(datensatz, index, array) {
			console.log(datensatz, index);
			//data.addRows([[new Date(datensatz[0], datensatz[1]-1, datensatz[2]), datensatz[3]]]);			
			time = datensatz[3]+":"+((datensatz[4]==0)?"00":datensatz[4])+" Uhr | "+ datensatz[5]+" C";
			//data.addRows([[new Date(datensatz[0], datensatz[1]-1, datensatz[2]), datensatz[6]]]);			
			data.addRows([[new Date(datensatz[0], datensatz[1]-1, datensatz[2], datensatz[3], datensatz[4]), datensatz[5], time]]);			
			
		});		

		titleDisplay = title.replace("WA_", "").replace("HE_", "").replace("_", " ").replace("_", " ");
		console.log(titleDisplay);
		
        // Set chart options
        var options = {'title':titleDisplay,
                       width:600,
                       height:300,
					   backgroundColor: 'transparent',
						hAxis: {
							format: ' HH.mm',
							title: 'Zeit'
						},
						vAxis: {
							title: 'C',
//							minValue:0
						}					   
					   };

        // Instantiate and draw our chart, passing in some options.
        var chart = new google.visualization.LineChart(document.getElementById('chart_div_'+title));
        chart.draw(data, options);
      }	  
	  
  
    </script>
	
	<script>	

 <?php 
/*
		$daten = getDBData($Datum);
		$einheit = "";

		echo "var daten_wwtemp = [";
		if (!empty($daten)) {
			while($row = $daten->fetch_assoc()) {
				//echo "Name: " . $row["Name"]. " - Datum: " . $row["Datum"]. " - " . $row["Wert"]. "- " . $row["Einheit"]. "<br>";
				$datumswert = explode(" ", $row["Datum"]);		
				$datum = explode("-", $datumswert[0]);
				$zeit = explode(":", $datumswert[1]);
				echo "[".$datum[0].", ".intval($datum[1]).", ".intval($datum[2]).", ".intval($zeit[0]).", ".intval($zeit[1]).", ".str_replace(",",".",$row["Temperatur"])."],";

			};
		}		
		echo "];\r\n";
		echo "google.charts.setOnLoadCallback(function () {\r\n";
		echo "drawChart('Warmwassertemperatur', daten_wwtemp);\r\n";
		echo "});\r\n";	   
*/		
		
		
		
 $werte = array(
	"HE_SET_TEMPERATURE_HC_1",
	"HE_ACTUAL_TEMPERATURE_HC_1",
	"HE_ACTUAL_FLOW_TEMPERATURE",
	"DH_ACTUAL_TEMPERATURE",
	"HE_SET_BUFFER_TEMPERATURE"
	);

	foreach($werte AS $wert) {
		$daten = getDBData($wert, $Datum);
		$einheit = "";
		$wert = str_replace("/","_",$wert);
		echo "var daten_".$wert." = [";
		if (!empty($daten)) {
			while($row = $daten->fetch_assoc()) {
				//echo "Name: " . $row["Name"]. " - Datum: " . $row["Datum"]. " - " . $row["Wert"]. "- " . $row["Einheit"]. "<br>";
				$datumswert = explode(" ", $row["Datum"]);
				$datum = explode("-", $datumswert[0]);
				$zeit = explode(":", $datumswert[1]);
				echo "[".$datum[0].", ".intval($datum[1]).", ".intval($datum[2]).", ".intval($zeit[0]).", ".intval($zeit[1]).", ".str_replace(",",".",$row["Wert"])."],";

				$einheit = $row["Einheit"];
			};
		}		
	   echo "];\r\n";
		echo "google.charts.setOnLoadCallback(function () {\r\n";
		echo "drawChart('".$wert."', 'C', daten_".$wert.");\r\n";
		echo "});\r\n";	   
	}			
?>
			
		</script>	
		
	
  </head>


  
  <body>
<?php include "navigation.php"; ?>

<div style="padding:20px; border-bottom:2px solid green; ">
<div style="display:inline-block; min-height:30px;">
	<form action="show_temp.php" method="post" style="display:inline;">
		<input type="hidden" name="Datum" value="<?php echo date( "Y-m-d", strtotime( strval($Datum) . "-1 day" ));?>" />
		<input type="submit" value=" - 1 Tag ">
	</form>

	<form action="show_temp.php" method="post" style="display:inline;padding-left:60px;padding-right:60px">
		<input type="date" name="Datum" value="<?php echo $Datum;?>" />
		<input type="submit" value=" Datums Auswahl ">
	</form>

	<form action="show_temp.php" method="post" style="display:inline;">
		<input type="hidden" name="Datum" value="<?php echo date( "Y-m-d", strtotime( strval($Datum) . "+1 day" ));?>" />
		<input type="submit" value=" + 1 Tag ">
	</form>

	
</div>
</div>

<div style="width:100%; border-bottom:2px solid green;text-align:center;">
<span style="width:400px; text-align:left;">Datum: <?php echo date("d.m.Y",strtotime($Datum)) ?></span>
</div>
  
    <!--Div that will hold the pie chart-->
		<div class="greenheading">Warmwasser</div>
    <div id="chart_div_DH_ACTUAL_TEMPERATURE" style="background-color:#DBF6FC;"></div>
	
	<div style="width:100%; display:inline-block; background-color:#E4E4E4;" >
		<div class="greenheading">Heizung</div>
		<div class="chart" id="chart_div_HE_SET_TEMPERATURE_HC_1"></div>
		<div class="chart" id="chart_div_HE_ACTUAL_TEMPERATURE_HC_1"></div>
		<div class="chart" id="chart_div_HE_ACTUAL_FLOW_TEMPERATURE"></div>
		<div class="chart" id="chart_div_HE_SET_BUFFER_TEMPERATURE"></div>
	</div>	
	</body>
</html>

<?php

function connectoToDB() {
	$servername = "localhost";
	$username = "isgweb";
	$password = "thisismypassword";
	$dbname = "isgweb";	
	
	$conn = new mysqli($servername, $username, $password, $dbname);
	if ($conn->connect_error) {
		die("Connection failed: " . $conn->connect_error);
	} 
	return $conn;
}


//Variablen for ISG Login
//The username or email address of the account.
define('USERNAME', 'username');
//The password of the account.
define('PASSWORD', 'password');

?>
